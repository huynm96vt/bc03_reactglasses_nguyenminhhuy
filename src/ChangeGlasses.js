import React, { Component } from 'react'
import mydata from './dataGlasses.json'


export default class ChangeGlasses extends Component {
 
state = {
  dataGlasses : {}
}

  selectGlasses = (data) => {
    let cloneDataGlasses = {...data}
    this.setState({dataGlasses : cloneDataGlasses})
  }

  render() {
      
    return (
      <div className='container'>
        <div style={{width: "300px", position: 'relative'}}>
          <img src={this.state.dataGlasses.url} alt="" style={{width: '150px', position: 'absolute', top: '100px', left : '75px'}} />
          <div style={{width: '100%', position: 'absolute', bottom: '0', backgroundColor:'rgba(100,100,100,0.5)'}}>
            <h1>{this.state.dataGlasses.name}</h1>
            <p>{this.state.dataGlasses.desc}</p>
          </div>
          <img src="./glassesImage/model.jpg" alt="" style={{width: '100%'}} />
        </div>
          <div className='row py-5'>
              {
                  mydata.map((data) => {
                      return <div onClick={() => {
                        this.selectGlasses(data)
                      }} className='col-2' key = {data.id}><img className='w-100' src={data.url} alt="" style={{cursor:"pointer"}} /></div>
                  })
              }
          </div>
      </div>
    )
  }
}
